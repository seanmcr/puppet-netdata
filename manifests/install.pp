# Private class for installing netdata packages.
#
# @summary Private class for installing netdata packages.
#
# @api private
#
# @example
#   include netdata::install
class netdata::install {
  if ($netdata::manage_package) {
    package { $netdata::package_name:
      ensure => $netdata::package_ensure,
    }
  }
}
